import Operaciones from './Operaciones.js'

export default class Calculadora extends Operaciones {
    constructor (){
        super()
        this.resultado = 0

        this.operandoA = undefined
        this.operandoB = undefined
        this.operador = undefined

        this.currentNumberEntered = ''
        
        this.negativeAFlag = false
        this.negativeBFlag = false
    }
    crearCalculadora () {
        var button, i, operacionActual, h1, main = document.createElement('div')
        
        h1 = document.createElement('h3')   
        h1.innerHTML = this.resultado

        for(i = 0; i < this.operaciones.length; i++){
            operacionActual = this.operaciones[i]
            button = document.createElement("button");
            button.setAttribute('name', operacionActual.nombre)
            button.setAttribute('value', operacionActual.valor || operacionActual.nombre)
            button.setAttribute('type', 'button')
            button.onclick = (event) => this.handleOperation(event.srcElement.value)
            button.innerHTML = operacionActual.nombre
            main.appendChild(button)
        }
        
        document.body.appendChild(h1)
        document.body.appendChild(main)
    }
    handleOperation(operation){
        var isNumber = !isNaN(operation)
        if(isNumber) {
            this.currentNumberEntered += operation
        } else if(operation === 'AC'){
            this.reset()
        } else if(operation === '='){
            this.operandoB = parseInt(this.currentNumberEntered) * (this.negativeBFlag ? -1 : 1)
            this.currentNumberEntered = ''
            this.resultado = this.operar(this.operandoA, this.operador, this.operandoB)
            this.operandoA = this.resultado
            this.operador = undefined
            this.negativeAFlag = false
            this.negativeBFlag = false
            document.body.childNodes[3].innerHTML = this.resultado
        } else {
            if(this.operandoA) {
                if(operation === '-' && this.operador) {
                    this.negativeBFlag = true
                } else {
                    this.operador = operation
                }
            } else {
                if(operation === '-' && !this.operandoA && !this.operandoB && this.currentNumberEntered === "") {
                    this.negativeAFlag = true
                } else {
                    this.operador = operation
                    this.operandoA = parseInt(this.currentNumberEntered)* (this.negativeAFlag ? -1 : 1)
                    this.currentNumberEntered = ''
                }
            }
        }
    }
    reset() {
        this.operandoA = undefined
        this.operandoB = undefined
        this.operador = undefined
        this.currentNumberEntered = ''
        this.resultado = 0
        this.negativeAFlag = false
        this.negativeBFlag = false
        document.body.childNodes[3].innerHTML = this.resultado
    }
}

