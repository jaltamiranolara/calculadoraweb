export default class Operaciones {
    constructor () {
        this.operaciones = [
            {
                nombre: 'AC'
            },
            {
                nombre: '='
            },
            {
                nombre: '+'
            },
            {
                nombre: '-'
            },
            {
                nombre: 'x',
                valor: '*'
            },
            {
                nombre: '\u00F7',
                valor: '/'
            },
            {
                nombre: '0',
                valor: 0
            },
            {
                nombre: '1',
                valor: 1
            },
            {
                nombre: '2',
                valor: 2
            },
            {
                nombre: '3',
                valor: 3
            },
            {
                nombre: '4',
                valor: 4
            },
            {
                nombre: '5',
                valor: 5
            },
            {
                nombre: '6',
                valor: 6
            },
            {
                nombre: '7',
                valor: 7
            },
            {
                nombre: '8',
                valor: 8
            },
            {
                nombre: '9',
                valor: 9
            }
        ]
    }
    operar(operandoA, operador, operandoB) {
        switch (operador) {
            case '+':
                return operandoA + operandoB
            case '-':
                return operandoA - operandoB
            case '*':
                return operandoA * operandoB
            case '/':
                return operandoA / operandoB
        }
    }
}